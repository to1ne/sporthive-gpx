# Sporthive GPX

This script is intended to convert a CSV file downloaded from a
[ProChip training](https://sporthive.com/dashboard/prochip#practice) to a GPX
file that can be uploaded to Strava.

## Deprecation warning

This script is deprecated as https://vinksite.com/Laps.htm can be used to easily
upload an activity directly to Strava from the website.
