require 'csv'
require 'erb'

class Coord
  attr_accessor :lat, :lon

  def initialize(lat:, lon:)
    @lat, @lon = lat, lon
  end

  def -(other)
    self.class.new(lat: lat - other.lat,
                   lon: lon - other.lon)
  end

  def +(other)
    self.class.new(lat: lat + other.lat,
                   lon: lon + other.lon)
  end
end

class Point
  attr_accessor :coord, :timestamp

  def initialize(coord, time)
    @coord, @timestamp = coord, time
  end
end

class Segment
  attr_accessor :start, :stop

  OutOfBounds = Class.new(StandardError)

  def initialize(start, stop, len)
    @start, @stop, @len = start, stop, len
  end

  private

  attr_accessor :len

  def between_at(dist)
    Coord.new(lat: offset(start.lat, stop.lat, dist),
              lon: offset(start.lon, stop.lon, dist))
  end

  def offset(a, b, dist)
    a + ((b - a) * dist / len)
  end
end

class Line < Segment
  def coord_at(distance)
    raise OutOfBounds.new('Distance not within segment') if distance > path_len

    between_at(distance)
  end

  def path_len
    len
  end
end

class Arc < Segment
  def coord_at(distance)
    raise OutOfBounds.new('Distance not within segment') if distance > path_len

    rad = Math::PI * distance / path_len
    cos = Math.cos(rad)
    sin = Math.sin(rad)
    lat = (start - center).lat
    lon = (start - center).lon

    Coord.new(lat: lon * sin + lat * cos,
              lon: lon * cos - lat * sin) + center
  end

  def path_len
    len * Math::PI / 2.0
  end

  def diam
    len
  end

  def center
    between_at(diam / 2.0)
  end
end

# Use https://www.gpsvisualizer.com/draw/ to determine 4 "corners"
# Aim for the straight to be 113m, and the arc a diameter of 55.38m
class Eindhoven
  def coords
    [
      Coord.new(lat: 51.414856, lon: 5.472436),
      Coord.new(lat: 51.414856, lon: 5.4732165),
      Coord.new(lat: 51.4156539, lon: 5.4732165),
      Coord.new(lat: 51.4156539, lon: 5.472436),

    ]
  end

  def segments
    [
      Arc.new(coords[0], coords[1], 55.38),
      Line.new(coords[1], coords[2], 88.73),
      Arc.new(coords[2], coords[3], 55.38),
      Line.new(coords[3], coords[0], 88.73),
    ]
  end
end

class Lap
  LENGTH = 400.0

  attr_accessor :duration, :elapsed

  def initialize(laptime)
    @duration = laptime_to_sec(laptime)
    @elapsed = 0.0
  end

  # Returns distance advanced over given seconds
  # or negative number of seconds remaining
  def advance(sec)
    dist = LENGTH * sec / duration

    self.elapsed += dist

    return dist if elapsed < LENGTH

    dist = LENGTH - elapsed
    duration * dist / LENGTH
  end

  # Rollback the given distance and return the seconds
  # of the rolled back distance
  def rollback(dist)
    self.elapsed -= dist

    duration * dist / LENGTH
  end

  private

  def laptime_to_sec(laptime)
    DateTime.strptime(laptime, '%k:%M:%S.%L').to_time.to_f -
      DateTime.strptime('0','%S').to_time.to_f
  end
end

class Session
  attr_accessor :track, :points, :start_time

  def initialize(track:)
    @track = track
    @points = []
  end

  def parse(file)
    timestamp = nil
    interval = 1.0

    CSV.foreach(file, headers: true, converters: %i[date date_time], encoding: 'utf-16le:utf-8') do |row|
      next unless row['Transponder']

      # unless timestamp
      #   timestamp = time_from(row)
      #   self.start_time = timestamp
      # end

      self.start_time = timestamp = time_from(row) unless timestamp

      lap = Lap.new(row['Laptime'])

      segment_dist = 0.0
      track.segments.each do |seg|
        loop do
          dist = lap.advance(interval)

          if dist < 0.0
            interval = -dist

            break
          end

          segment_dist += dist

          if segment_dist > seg.path_len
            segment_dist -= seg.path_len
            interval = lap.rollback(segment_dist)

            break
          end

          points << Point.new(seg.coord_at(segment_dist), timestamp)
          timestamp += 1.0 / (24.0 * 3600.0)

          interval = 1.0
        end

        # return self # for single arc
      end

      # break # for single lap
    end

    self
  end

  private

  def time_from(row)
    date = Date.strptime(row['Date'], '%d-%m-%Y')
    time = Date.strptime(row['Start time'], '%H:%M:%s').to_time

    DateTime.new(date.year, date.month, date.day, time.hour, time.min, time.sec, 'CET')
  end
end


session = Session.new(track: Eindhoven.new).parse('report.csv')

template = <<~EOF
<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Sporthive GPX" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" version="1.1" xmlns="http://www.topografix.com/GPX/1/1">
  <metadata>
    <time><%= session.start_time.strftime('%FT%TZ') %></time>
  </metadata>
  <trk>
    <name>Ice skate</name>
    <type>5</type>
    <trkseg><% session.points.each do |point| %>
      <trkpt lat="<%= point.coord.lat %>" lon="<%= point.coord.lon %>">
        <time><%= point.timestamp.strftime('%FT%TZ') %></time>
      </trkpt><% end %>
    </trkseg>
  </trk>
</gpx>
EOF

puts ERB.new(template).result_with_hash(session: session)
